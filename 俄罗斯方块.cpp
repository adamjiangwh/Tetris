#include <easyx.h>
#include <conio.h>
#include<stdlib.h>
#include<stdio.h>
#include <time.h>
#include <graphics.h>

#pragma comment(lib,"Winmm.lib")

#define	WIDTH	15		// 游戏区宽度
#define	HEIGHT	25		// 游戏区高度
#define	UNIT	20		// 每个游戏区单位的实际像素

// 定义操作类型
enum CMD
{
	CMD_ROTATE,						// 方块旋转
	CMD_LEFT, CMD_RIGHT, CMD_DOWN,	// 方块左、右、下移动
	CMD_SINK,						// 方块沉底
	CMD_QUIT						// 退出游戏
};

// 定义绘制方块的方法
enum DRAW
{
	SHOW,	// 显示方块
	CLEAR,	// 擦除方块
	FIX		// 固定方块
};

// 定义七种俄罗斯方块
struct BLOCK
{
	WORD dir[4];	// 方块的四个旋转状态
	COLORREF color;	// 方块的颜色
}	g_Blocks[7] = {	{0x0F00, 0x4444, 0x0F00, 0x4444, LIGHTRED},		// I
					{0x0660, 0x0660, 0x0660, 0x0660, LIGHTBLUE},		// 口
					{0x4460, 0x02E0, 0x0622, 0x0740, LIGHTMAGENTA},	// L
					{0x2260, 0x0E20, 0x0644, 0x0470, YELLOW},	// 反L
					{0x0C60, 0x2640, 0x0C60, 0x2640, LIGHTCYAN},		// Z
					{0x0360, 0x4620, 0x0360, 0x4620, LIGHTGREEN},	// 反Z
					{0x4E00, 0x4C40, 0x0E40, 0x4640, BROWN}};	// T

// 定义当前方块、下一个方块的信息
struct BLOCKINFO
{
	byte id;	// 方块 ID
	char x, y;	// 方块在游戏区中的坐标
	byte dir:2;	// 方向
}	g_CurBlock, g_NextBlock;

// 定义游戏区
BYTE g_World[WIDTH][HEIGHT] = {0};


// 函数声明

void Init();											// 初始化游戏
void Quit();											// 退出游戏
void NewGame();											// 开始新游戏
void GameOver();										// 结束游戏
CMD  GetCmd();											// 获取控制命令
void DispatchCmd(CMD _cmd);								// 分发控制命令
void NewBlock();										// 生成新的方块
bool CheckBlock(BLOCKINFO _block);						// 检测指定方块是否可以放下
void DrawUnit(int x, int y, COLORREF c, DRAW _draw);	// 画单元方块
void DrawBlock(BLOCKINFO _block, DRAW _draw = SHOW);	// 画方块
void OnRotate();										// 旋转方块
void OnLeft();											// 左移方块
void OnRight();											// 右移方块
void OnDown();											// 下移方块
void OnSink();											// 沉底方块
void UpdataScore();										// 计算分数
void DrawScore();										// 绘制分数

int g_score;

// 主函数
void main()
{
	Init();
	CMD c;
	while(true)
	{
		c = GetCmd();
		DispatchCmd(c);

		// 按退出时，显示对话框咨询用户是否退出
		if (c == CMD_QUIT)
		{
			HWND wnd = GetHWnd();
			if (MessageBox(wnd, _T("Are you sure to quit the game？"), _T("tips"), MB_OKCANCEL | MB_ICONQUESTION) == IDOK)
				Quit();
		}
	}
}

void DrawScore()
{
	char scoreStr[5];
	if(g_score<10)
		_stprintf(scoreStr ,_T("0000%d") ,g_score);
	else if(g_score<100)
		_stprintf(scoreStr ,_T("000%d") ,g_score);
	else if(g_score<1000)
		_stprintf(scoreStr ,_T("00%d") ,g_score);
	else if(g_score<10000)
		_stprintf(scoreStr ,_T("0%d") ,g_score);
	else if(g_score<100000)
		_stprintf(scoreStr ,_T("%d") ,g_score);
	else
		_stprintf(scoreStr ,_T("%d") ,99999);
	settextstyle(25, 0, _T("宋体"));
	settextcolor(LIGHTRED);
    outtextxy(350 ,185 ,scoreStr);
}

// 初始化游戏
void Init()
{
	initgraph(700, 550);
	srand((unsigned)time(NULL));
	setbkmode(TRANSPARENT);			// 设置图案填充的背景色为透明
	
	char scoreStr[5];

	setcolor(LIGHTCYAN);
	outtextxy(550, 25, _T("下一个方块："));
	//显示title
	loadimage(NULL, _T("IMAGE"), _T("Name"));
	
	//加载音乐
	mciSendString("open res\\music.mp3 alias mymusic", NULL, 0, NULL);
	mciSendString("play mymusic repeat", NULL, 0, NULL);
	// 显示操作说明
	settextstyle(14, 0, _T("宋体"));
	outtextxy(550, 270, _T("操作方法:"));
	//outtextxy(550, 270, _T(""));
	outtextxy(550, 300, _T("↑/W：旋转"));
	outtextxy(550, 320, _T("←/A：左移"));
	outtextxy(550, 340, _T("→/D：右移"));
	outtextxy(550, 360, _T("↓/S：下移"));
	outtextxy(550, 380, _T("SPACE：下落"));
	outtextxy(550, 400, _T("P：暂停/继续"));
	outtextxy(550, 420, _T("ESC：退出"));

	
	//显示分数
	g_score=0;
	settextstyle(25, 0, _T("宋体"));
	setcolor(LIGHTCYAN);
	outtextxy(575, 165, _T("得分"));
	settextcolor(LIGHTRED);
	_stprintf(scoreStr ,_T("0000%d") ,g_score);
    outtextxy(569 ,205 ,scoreStr);
	//DrawScore();

	// 显示版权信息
	settextstyle(14, 0, _T("宋体"));
	setcolor(YELLOW);
	outtextxy(550, 460, _T("开发者  姜文昊"));
	outtextxy(550, 490, _T(" 2 + 2 实验室 "));


	// 设置坐标原点
	setorigin(220, 20);

	setcolor(LIGHTGREEN);
	
	// 绘制游戏区边界

	rectangle(-1, -1, WIDTH * UNIT, HEIGHT * UNIT);
	//rectangle((WIDTH + 1) * UNIT - 10, 20, (WIDTH + 5) * UNIT, 4 * UNIT+20);

	//绘制操作说明边界
	rectangle(WIDTH * UNIT +15, 0, WIDTH * UNIT +150, HEIGHT * UNIT - 370);
	rectangle(WIDTH * UNIT +15, 235, WIDTH * UNIT +150, HEIGHT * UNIT - 365);
	rectangle(WIDTH * UNIT +15, 240, WIDTH * UNIT +150, HEIGHT * UNIT - 80);
	rectangle(WIDTH * UNIT +15, 500, WIDTH * UNIT +150, HEIGHT * UNIT - 75);
	

	//setlinestyle(PS_DASH, 2);
	//line((WIDTH + 0.5) * UNIT - 1, 0, (WIDTH +0.45) * UNIT, HEIGHT*20);
	//line((WIDTH +0.5) * UNIT - 1, 10, (WIDTH +15) * UNIT, HEIGHT*20-65);
	// 开始新游戏
	NewGame();
}


// 退出游戏
void Quit()
{
	closegraph();
	exit(0);
}


// 开始新游戏
void NewGame()
{
	setfillcolor(BLACK);
	solidrectangle(340, 175, WIDTH * UNIT +120, HEIGHT * UNIT -270);
	g_score=0;
	DrawScore();

	// 清空游戏区
	setfillcolor(BLACK);
	solidrectangle(0, 0, WIDTH * UNIT - 1, HEIGHT * UNIT - 1);
	ZeroMemory(g_World, WIDTH * HEIGHT);

	// 生成下一个方块
	g_NextBlock.id = rand() % 7;
	g_NextBlock.dir = rand() % 4;
	g_NextBlock.x = WIDTH + 2;
	g_NextBlock.y = HEIGHT - 3;


	// 获取新方块
	NewBlock();
}


// 结束游戏
void GameOver()
{
	char gameover[100];
	RECT location={0,0,290,410};
	//settextstyle(50,0,NULL);
	LOGFONT f;
	gettextstyle(&f);
	f.lfHeight = 48;
	_tcscpy(f.lfFaceName ,_T("宋体"));
	f.lfQuality = ANTIALIASED_QUALITY;
	settextstyle(&f);
	setcolor(LIGHTRED);
	drawtext("GAME OVER!",&location,DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	HWND wnd = GetHWnd();
	sprintf(gameover,"Score:%d\nDo you want to try again？",g_score);
	if (MessageBox(wnd, gameover, _T("Game Over!"), MB_YESNO | MB_ICONQUESTION) == IDYES)
		NewGame();
	else
		Quit();
}


// 获取控制命令
DWORD m_oldtime;
CMD GetCmd()
{
	// 获取控制值
	while(true)
	{
		// 如果超时，自动下落一格
		DWORD newtime = GetTickCount();
		if (newtime - m_oldtime >= 500)
		{
			m_oldtime = newtime;
			return CMD_DOWN;
		}

		// 如果有按键，返回按键对应的功能
		if (kbhit())
		{
			switch(getch())
			{
				case 'w':
				case 'W':	return CMD_ROTATE;
				case 'a':
				case 'A':	return CMD_LEFT;
				case 'd':
				case 'D':	return CMD_RIGHT;
				case 's':
				case 'S':	return CMD_DOWN;
				case 'p':
				case 'P':	system("pause");break;
				case 27:	return CMD_QUIT;
				case ' ':	return CMD_SINK;
				case 0:
				case 0xE0:
					switch(getch())
					{
						case 72:	return CMD_ROTATE;
						case 75:	return CMD_LEFT;
						case 77:	return CMD_RIGHT;
						case 80:	return CMD_DOWN;
					}
			}
		}

		// 延时
		Sleep(20);
	}
}


// 分发控制命令
void DispatchCmd(CMD _cmd)
{
	switch(_cmd)
	{
		case CMD_ROTATE:	OnRotate();		break;
		case CMD_LEFT:		OnLeft();		break;
		case CMD_RIGHT:		OnRight();		break;
		case CMD_DOWN:		OnDown();		break;
		case CMD_SINK:		OnSink();		break;
		case CMD_QUIT:		break;
	}
}


// 生成新的方块
void NewBlock()
{
	g_CurBlock.id = g_NextBlock.id,		g_NextBlock.id = rand() % 7;
	g_CurBlock.dir = g_NextBlock.dir,	g_NextBlock.dir = rand() % 4;
	g_CurBlock.x = (WIDTH - 4) / 2;
	g_CurBlock.y = HEIGHT + 2;


	// 下移新方块直到有局部显示
	WORD c = g_Blocks[g_CurBlock.id].dir[g_CurBlock.dir];
	while((c & 0xF) == 0)
	{
		g_CurBlock.y--;
		c >>= 4;
	}

	// 绘制新方块
	DrawBlock(g_CurBlock);

	// 绘制下一个方块
	setfillcolor(BLACK);
	solidrectangle((WIDTH + 1) * UNIT, 25, (WIDTH + 5) * UNIT +20, 4 * UNIT - 1+40);
	DrawBlock(g_NextBlock);

	// 设置计时器，用于判断自动下落
	m_oldtime = GetTickCount();
}


// 画单元方块
void DrawUnit(int x, int y, COLORREF c, DRAW _draw)
{
	// 计算单元方块对应的屏幕坐标
	int left = x * UNIT;
	int top = (HEIGHT - y - 1) * UNIT;
	int right = (x + 1) * UNIT - 1;
	int bottom = (HEIGHT - y) * UNIT - 1;

	// 画单元方块
	switch(_draw)
	{
		case SHOW:
			// 画普通方块
			setlinecolor(0x006060);
			roundrect(left + 10, top + 1, right - 1, bottom - 1, 5, 5);
			setlinecolor(0x003030);
			roundrect(left, top, right, bottom, 8, 8);
			setfillcolor(c);
			setlinecolor(LIGHTGRAY);
			fillrectangle(left + 2, top + 2, right - 2, bottom - 2);
			break;

		case FIX:
			// 画固定的方块
			//setfillcolor(RGB(GetRValue(c) * 2 / 3, GetGValue(c) * 2 / 3, GetBValue(c) * 2 / 3));
			setlinecolor(LIGHTGRAY);
			setfillcolor(DARKGRAY);
			fillrectangle(left + 1, top + 1, right - 1, bottom - 1);
			break;

		case CLEAR:
			// 擦除方块
			setfillcolor(BLACK);
			solidrectangle(x * UNIT, (HEIGHT - y - 1) * UNIT, (x + 1) * UNIT - 1, (HEIGHT - y) * UNIT - 1);
			break;
	}
}


// 画方块
void DrawBlock(BLOCKINFO _block, DRAW _draw)
{
	WORD b = g_Blocks[_block.id].dir[_block.dir];
	int x, y;

	for(int i = 0; i < 16; i++, b <<= 1)
		if (b & 0x8000)
		{
			x = _block.x + i % 4;
			y = _block.y - i / 4;
			if (y < HEIGHT)
				DrawUnit(x, y, g_Blocks[_block.id].color, _draw);
		}
}


// 检测指定方块是否可以放下
bool CheckBlock(BLOCKINFO _block)
{
	WORD b = g_Blocks[_block.id].dir[_block.dir];
	int x, y;

	for(int i = 0; i < 16; i++, b <<= 1)
		if (b & 0x8000)
		{
			x = _block.x + i % 4;
			y = _block.y - i / 4;
			if ((x < 0) || (x >= WIDTH) || (y < 0))
				return false;

			if ((y < HEIGHT) && (g_World[x][y]))
				return false;
		}

	return true;
}


// 旋转方块
void OnRotate()
{
	// 获取可以旋转的 x 偏移量
	int dx;
	BLOCKINFO tmp = g_CurBlock;
	tmp.dir++;					if (CheckBlock(tmp))	{	dx = 0;		goto rotate;	}
	tmp.x = g_CurBlock.x - 1;	if (CheckBlock(tmp))	{	dx = -1;	goto rotate;	}
	tmp.x = g_CurBlock.x + 1;	if (CheckBlock(tmp))	{	dx = 1;		goto rotate;	}
	tmp.x = g_CurBlock.x - 2;	if (CheckBlock(tmp))	{	dx = -2;	goto rotate;	}
	tmp.x = g_CurBlock.x + 2;	if (CheckBlock(tmp))	{	dx = 2;		goto rotate;	}
	return;

rotate:
	// 旋转
	DrawBlock(g_CurBlock, CLEAR);
	g_CurBlock.dir++;
	g_CurBlock.x += dx;
	DrawBlock(g_CurBlock);

}


// 左移方块
void OnLeft()
{
	BLOCKINFO tmp = g_CurBlock;
	tmp.x--;
	if (CheckBlock(tmp))
	{
		DrawBlock(g_CurBlock, CLEAR);
		g_CurBlock.x--;
		DrawBlock(g_CurBlock);
	}
}


// 右移方块
void OnRight()
{
	BLOCKINFO tmp = g_CurBlock;
	tmp.x++;
	if (CheckBlock(tmp))
	{
		DrawBlock(g_CurBlock, CLEAR);
		g_CurBlock.x++;
		DrawBlock(g_CurBlock);
	}
}


// 下移方块
void OnDown()
{
	BLOCKINFO tmp = g_CurBlock;
	tmp.y--;
	if (CheckBlock(tmp))
	{
		DrawBlock(g_CurBlock, CLEAR);
		g_CurBlock.y--;
		DrawBlock(g_CurBlock);
	}
	else
		OnSink();	// 不可下移时，执行“降落方块”操作
}


// 降落方块
void OnSink()
{
	int i, x, y;

	// 连续下移方块
	DrawBlock(g_CurBlock, CLEAR);
	BLOCKINFO tmp = g_CurBlock;
	tmp.y--;
	while (CheckBlock(tmp))
	{
		g_CurBlock.y--;
		tmp.y--;
	}
	DrawBlock(g_CurBlock, FIX);

	// 固定方块在游戏区
	WORD b = g_Blocks[g_CurBlock.id].dir[g_CurBlock.dir];
	for(i = 0; i < 16; i++, b <<= 1)
		if (b & 0x8000)
		{
			if (g_CurBlock.y - i / 4 >= HEIGHT)
			{	// 如果方块的固定位置超出高度，结束游戏
				GameOver();
				return;
			}
			else
				g_World[g_CurBlock.x + i % 4][g_CurBlock.y - i / 4] = 1;
		}

	// 检查是否需要消掉行，并标记
	BYTE remove = 0;
	for(y = g_CurBlock.y; y >= max(g_CurBlock.y - 3, 0); y--)
	{
		i = 0;
		for(x = 0; x < WIDTH; x++)
			if (g_World[x][y] == 1)
				i++;

		if (i == WIDTH)
		{
			remove |= (1 << (g_CurBlock.y - y));
			//清空分数
			setfillcolor(BLACK);
			solidrectangle(340, 175, WIDTH * UNIT +120, HEIGHT * UNIT -270);
			g_score+=10;
			DrawScore();
			//UpdataScore();
			//setfillcolor(BLACK);
			//setlinecolor(WHITE);
			//setfillstyle(BS_HATCHED, BS_HOLLOW);
			//fillrectangle(0, (HEIGHT - y - 1) * UNIT + UNIT / 2 - 5, WIDTH * UNIT - 1, (HEIGHT - y - 1) * UNIT + UNIT / 2 + 5);
			//setfillstyle(BS_SOLID);
		}
	}

	if (remove)	// 如果产生整行消除
	{
		// 延时 300 毫秒
		Sleep(300);

		// 擦掉刚才标记的行
		IMAGE img;
		for(i = 0; i < 4; i++, remove >>= 1)
		{
			if (remove & 1)
			{
				for(y = g_CurBlock.y - i + 1; y < HEIGHT; y++)
					for(x = 0; x < WIDTH; x++)
					{
						g_World[x][y - 1] = g_World[x][y];
						g_World[x][y] = 0;
					}

				getimage(&img, 0, 0, WIDTH * UNIT, (HEIGHT - (g_CurBlock.y - i + 1)) * UNIT);
				putimage(0, UNIT, &img);
			}
		}
	}

	// 产生新方块
	NewBlock();
}

//分数计算
/*void UpdataScore()
{  
    char scoreStr[5]; //用字符串的形式存储得分  
    //int g_score=0;  
    //extern int g_grade ; 
	
    //分数的增长的单位是10  
    g_score += 10 ;  
      
    //删除原先信息  
    //setfillcolor(BLACK);   
    //bar(WIDTH * UNIT +16, 230, WIDTH * UNIT +145, HEIGHT * UNIT - 330); 
    
    //显示信息  
    settextstyle(25, 0, _T("宋体"));
	settextcolor(LIGHTCYAN);
	_stprintf(scoreStr ,_T("%d") ,g_score);
	
	outtextxy(575, 165, _T("得分"));
	settextcolor(LIGHTRED);
    outtextxy(590 ,205 ,scoreStr);
}  */